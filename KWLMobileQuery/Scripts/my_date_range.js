﻿$(function () {
    const $dateControl = $('#dateControl');
    const $form = $dateControl.find('#searchForm');
    const $searchFrom = $dateControl.find('#SearchFrom');
    const $searchTo = $dateControl.find('#SearchTo');
    const $level = $('#dateControl').find('#SearchLevel');

    $level.append('<option></option>');
    selectList.forEach(x => {
        if (defaultLevel === x) {
            $level.append('<option selected>' + x + '</option>');
        } else {
            $level.append('<option>' + x + '</option>');
        }
    });

    
    $searchFrom.datetimepicker({
        date: moment(defaultFrom, 'DD/MM/YYYY HH:mm:ss a'),
        maxDate: moment(Date.now()),
        showTodayButton : true,
        sideBySide: true,
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    });

    
    $searchTo.datetimepicker({
        date: moment(defaultTo, 'DD/MM/YYYY HH:mm:ss a'),
        maxDate: moment(Date.now()),
        showTodayButton: true,
        sideBySide: true,
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    });

    

    
    $form.submit(function (e) {
        e.preventDefault();

        var ms = moment($searchTo.find('input').val()).diff(moment($searchFrom.find('input').val()));

        if (ms === 0) alert("To Date and From Date cannot be the same!");
        if(ms < 0)  alert("To Date must be greater than the From Date!");

        const QueryStr = '?fr=' + encodeURIComponent($searchFrom.find('input').val()) +
            '&to=' + encodeURIComponent($searchTo.find('input').val()) +
            '&sl=' + $level.val();
        window.location.href = window.location.protocol +
            "//" + window.location.host + window.location.pathname + QueryStr;
    });

});