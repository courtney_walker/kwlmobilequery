﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KWLMobileQuery.Classes
{
    public static class UserCls
    {

        public static Connections.AspNetUser i_user;
        private static Connections.UserManagementAPIEntities _userDb = new Connections.UserManagementAPIEntities(); 
        

        public static void setUserByName(string userName)
        {
            i_user = (from p in _userDb.AspNetUsers where p.UserName == userName select p).FirstOrDefault();
        }


        public static void setUserById(string userId)
        {
            i_user = (from p in _userDb.AspNetUsers where p.Id == userId select p).FirstOrDefault();
        }        

    }
}