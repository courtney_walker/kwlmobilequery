﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KWLMobileQuery.Connections;
using KWLMobileQuery.Models;

namespace KWLMobileQuery.Controllers
{
    [Authorize]
    public class NLogsController : Controller
    {
        private UserManagementAPIEntities db = new UserManagementAPIEntities();

        // GET: NLogs
        public ActionResult Index()
        {
            return View((from p in db.NLogs
                         where p.UserName == Classes.UserCls.i_user.UserName
                         orderby p.ID descending
                         select p).Take(50).ToList());
        }

       
        public ActionResult Anon()
        {
            string frTimeStr = Request.QueryString["fr"];
            string toTimeStr = Request.QueryString["to"];
            string sl = Request.QueryString["sl"];

            DateTime fromTime;
            var isValidFromDate = DateTime.TryParseExact(frTimeStr, "MM/dd/yyyy h:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out fromTime);
            if (!isValidFromDate) fromTime = DateTime.Now;

            DateTime toTime;
            var isValidToDate = DateTime.TryParseExact(toTimeStr, "MM/dd/yyyy h:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out toTime);
            if (!isValidToDate) toTime = DateTime.Now;

            ViewBag.SearchFrom = (DateTime)fromTime;
            ViewBag.SearchTo = (DateTime)toTime;

            ViewBag.Levels = "Error,Info";
            ViewBag.level = sl;

            var xlist = db.NLogs.Where(p => p.Logged >= fromTime && p.Logged <= toTime &&
                                          (p.UserName == null || p.UserName.Equals(string.Empty)));
                                   

            if (!String.IsNullOrEmpty(sl))
            {
               xlist =  xlist.Where(p => p.Level == sl);
            }

            return View(xlist.OrderByDescending(p=>p.Logged).Take(100).ToList());


        }

        // GET: NLogs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NLog nLog = db.NLogs.Find(id);
            if (nLog == null)
            {
                return HttpNotFound();
            }
            return View(nLog);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
