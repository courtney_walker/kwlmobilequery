﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KWLMobileQuery.Connections;

namespace KWLMobileQuery.Controllers
{
    [Authorize]
    public class PaymentsController : Controller
    {
        private PaymentsAPI_QAEntities db = new PaymentsAPI_QAEntities();

        // GET: Payments
        public ActionResult Index()
        {
            return View(db.Payments.Where(p=>p.UserId == Classes.UserCls.i_user.Id).OrderByDescending(p=>p.CreatedAt).Take(100).ToList());
        }

        // GET: Payments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payment payment = db.Payments.Find(id);
            if (payment == null)
            {
                return HttpNotFound();
            }            
            ViewBag.ParentUrl = @Url.Action("Index", "Payments");
            ViewBag.ParentTitle = "Payments";
            return View(payment);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
