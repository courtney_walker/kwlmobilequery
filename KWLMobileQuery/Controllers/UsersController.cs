﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KWLMobileQuery.Classes;
using KWLMobileQuery.Connections;

namespace KWLMobileQuery.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private UserManagementAPIEntities db = new UserManagementAPIEntities();

        // GET: Users
        public ActionResult Index(string id)
        {
            UserCls.i_user = null;
            var query = new List<AspNetUser>();
            if (!string.IsNullOrEmpty(id))
            {
                query = db.AspNetUsers.Where(p => p.UserName.Contains(id) ||
                                                  p.FirstName.Contains(id)).ToList();
            }

            if (query.Count > 10)
            {
                ModelState.AddModelError("Error", "Ex: You have to be more specific");
                query = new List<AspNetUser>();
            }
            else if (query.Count == 1)
            {
                return RedirectToAction("Details", "Users", new { id = query.First().Id });
            }

            return View(query);

        }

        public ActionResult showUsers(string term)
        {
            var query = db.AspNetUsers.Where(p => p.UserName.Contains(term))
                                      .Select(p => p.UserName).ToList();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: AspNetUsers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserCls.setUserById(id);
            if (UserCls.i_user == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserDetailView = true;
            return View(UserCls.i_user);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
