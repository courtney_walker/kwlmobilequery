﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KWLMobileQuery.Connections;

namespace KWLMobileQuery.Controllers
{
    [Authorize]
    public class LogsController : Controller
    {
        private APMServicesProxyAPIEntities db = new APMServicesProxyAPIEntities();

        // GET: Logs
        public ActionResult Index()
        {
            string frTimeStr = Request.QueryString["fr"];
            string toTimeStr = Request.QueryString["to"];
            string sl = Request.QueryString["sl"];

            DateTime fromTime;
            var isValidFromDate = DateTime.TryParseExact(frTimeStr, "MM/dd/yyyy h:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out fromTime);
            if (!isValidFromDate) fromTime = DateTime.Now;

            DateTime toTime;
            var isValidToDate = DateTime.TryParseExact(toTimeStr, "MM/dd/yyyy h:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out toTime);
            if (!isValidToDate) toTime = DateTime.Now;
            
            ViewBag.SearchFrom =(DateTime)fromTime;
            ViewBag.SearchTo = (DateTime)toTime;


            ViewBag.Levels = "Warning,Information,Debug,Error,Fatal";
            ViewBag.level = sl;

            var xlist = db.Logs.Where(p => p.TimeStamp >= fromTime && p.TimeStamp <= toTime);
            if (!String.IsNullOrEmpty(sl))
            {
                xlist = xlist.Where(p=>p.Level == sl);
            }
            return View(xlist.OrderByDescending(p=>p.TimeStamp).Take(100).ToList());
        }

        // GET: Logs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Log log = db.Logs.Find(id);
            if (log == null)
            {
                return HttpNotFound();
            }
            ViewBag.ParentUrl = @Url.Action("Index", "Logs");
            ViewBag.ParentTitle = "APM Logs";
            return View(log);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
